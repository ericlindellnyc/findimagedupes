module gitlab.com/opennota/findimagedupes

require (
	github.com/mattn/go-sqlite3 v1.9.0
	github.com/rakyll/magicmime v0.1.0
	gitlab.com/opennota/phash v0.0.0-20180911054150-409761801c2b
)
